# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Contributor: Helmut Stult
# Contributor: Bruno Pagani <archange@archlinux.org>
# Contributor: Eli Schwartz <eschwartz@archlinux.org>
# Contributor: Alexandre Filgueira <alexfilgueira@cinnarch.com>
# Contributor: M0Rf30
# Contributor: unifiedlinux
# Contributor: CReimer

pkgname=cinnamon
pkgver=6.4.8
pkgrel=1
pkgdesc="Linux desktop which provides advanced innovative features and a traditional user experience"
arch=('x86_64')
url="https://github.com/linuxmint/cinnamon"
license=('GPL-2.0-or-later')
depends=(
  'accountsservice'
  'caribou'
  'cinnamon-control-center'
  'cinnamon-desktop'
  'cinnamon-menus'
  'cinnamon-screensaver'
  'cinnamon-session'
  'cinnamon-settings-daemon'
  'cjs'
  'gcr'
  'gdk-pixbuf2'
  'glib2'
  'gnome-backgrounds'
  'gsound'
  'gstreamer'
  'gtk3'
  'libgnomekbd'
  'libkeybinder3'
  'librsvg'
  'libsecret'
  'muffin'
  'network-manager-applet'
  'nemo'
  'pango'
  'polkit-gnome'
  'python'
  'python-cairo'
  'python-dbus'
  'python-gobject'
  'python-pam'
  'python-pexpect'
  'python-pillow'
  'python-pyinotify'
  'python-pytz'
  'python-requests'
  'python-tinycss2'
  'python-xapp'
  'timezonemap'
  'xapps'
  'xdg-desktop-portal-xapp'
)
optdepends=(
  'blueman: Bluetooth support'
  'cinnamon-translations: i18n'
  'gnome-online-accounts-gtk'
  'gnome-panel: fallback mode'
  'gnome-terminal: X terminal emulator'
  'metacity: fallback mode'
  'switcheroo-control: GPU offloading'
  'system-config-printer: printer settings'
  'touchegg: touch gestures'
  'wget: cover download support in audio applet'
)
makedepends=(
  'glib2-devel'
  'gobject-introspection'
  'intltool'
  'meson'
  'python-libsass'
  'samurai'
)
options=('!emptydirs')
source=("${url}/archive/${pkgver}/${pkgname}-${pkgver}.tar.gz"
        'set_wheel.diff'
        'default-theme.patch'
        'manjaro-settings.patch')
sha512sums=('11e929ca17a80c9aeaf92138333a717f8702cc27669a03d4a95cb8b4154b65869ed102a91a303ce80901667a343e5ed5647a7eff216f630dcd6ff44a5101fa26'
            'fd7e117054996ed1c3dfd0f968c2bf98ca4fcee9a100221f8839a232147745ec0140e1f68eeffba58a3c44f66f26e05d433648a7a28858ec669524f7266ba04c'
            'ee7dedd59ea370cf81d75def49060f9a29b22e7b025ca7d5db87a0102d50f138c79aa562b0d36a748c4b1c59a37f600ba1f60ff6caf303cf5b6fc4d110d051b4'
            '479b13504b2726129693983dba30b80d2a6cab9bef7a6d3091c0d533be22d8279d4171ce39befa1680c909e1df7ea6631d57a96d5641dd2c9111b355f7d92005')
b2sums=('165f4e89844af4c58d85d8341542c86891c2f7f3bd56bc1a6f6b14bd2357bb8f3884452defdd99202e5f63efb817ca3b9737d884c40ee87912b0cd82898fb1db'
        '3becf1f40068fc629109e6e7d464c3c484296afacc9ab6328b2ccbb3c9735bcbfa9550f9f73b430ede178ae668e37c660ce322b5b4d1873526de3d3d41185160'
        '86c3a29acd132ca321f08fd81dd5a45707accdc035b2aeec95bf358b29072ff1eedb77b2566cf48d7253d1d791599f0f44938c4600761d711cb18b59019f1c62'
        'eba1c9660c68916e70ca68c4de73d6f170c534cbdc71c7a11ae4f7c86d73e210d703967c63bb76c3c0a331384c6b928076f284c3c7de1560d561b8eb0dd7bc79')

prepare() {
  cd "${pkgname}-${pkgver}"

  # Use wheel group instead of sudo (taken from Fedora)
  patch -p1 < ../set_wheel.diff

  # Set default theme to 'cinnamon'
  patch -p1 < ../default-theme.patch

  # Replace MintInstall with Pamac
  sed -i 's/mintinstall.desktop/org.manjaro.pamac.manager.desktop/' data/org.cinnamon.gschema.xml

  # Add polkit agent to required components
  sed -i 's/RequiredComponents=\(.*\)$/RequiredComponents=\1polkit-gnome-authentication-agent-1;/' \
      cinnamon*.session.in

  # https://github.com/linuxmint/cinnamon/issues/3575#issuecomment-374887122
  # Cinnamon has no upstream backgrounds, use GNOME backgrounds instead
  sed -i 's|/usr/share/cinnamon-background-properties|/usr/share/gnome-background-properties|' \
      files/usr/share/cinnamon/cinnamon-settings/modules/cs_backgrounds.py

  # add MSM to cinnamon-settings
  patch -Np1 -i ../manjaro-settings.patch
}

build() {
  arch-meson --libexecdir=lib/cinnamon "${pkgname}-${pkgver}" build
  samu -C build
}

package() {
  DESTDIR="${pkgdir}" samu -C build install
}
